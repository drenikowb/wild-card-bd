﻿#pragma strict

import System.Collections.Generic;

function Update () {

	var mousePosition = Camera.main.ScreenPointToRay(Input.mousePosition);

	var direction = Vector2(mousePosition.origin.x-transform.position.x,mousePosition.origin.y-transform.position.y);

	transform.right = -direction;
}
