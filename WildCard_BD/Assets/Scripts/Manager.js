﻿#pragma strict
import System.Collections.Generic;

var cardPrefab: GameObject;
var manaPrefab: GameObject;
var mainCanvas: Transform;

static var manaAvailable: int;
static var totalMana: int;
static var NewTurn: boolean;

private var handList : List.<GameObject>;
private var enemyHandList : List.<GameObject>;
private var counter: int;
private var newCard: GameObject;
private var allCards: Object[];
private var firstTurn: boolean;
private var openingHandSize: int;
private var maximumHandSize: int;

function Start () {
	handList = new List.<GameObject>();
	enemyHandList = new List.<GameObject>();

	allCards = Resources.LoadAll("ScriptableObjects",Card);

	//CreateCard(0);
	var newMana = Instantiate(manaPrefab, Vector3.zero, Quaternion.identity);
	newMana.transform.SetParent(GameObject.Find("ManaTray").transform);
	manaAvailable = 1;
	totalMana = 1;
	firstTurn = true;
	NewTurn = false;
	openingHandSize = 3;
	maximumHandSize = 10;
}

function Update () {
	if(counter > 100 && NewTurn == true && GameObject.Find("Hand").transform.childCount < maximumHandSize)
	{
		CreateCard();
		NewTurn = false;
	}
	else if(counter > 100 && handList.Count < openingHandSize && firstTurn == true)
	{
		var enemyHand = GameObject.Find("EnemyHand");
		CreateCard(Vector3(0,enemyHand.transform.position.y,0));
		CreateCard();
		counter = 0;
	}
	else if (handList.Count >= openingHandSize)
	{
		firstTurn = false;
		NewTurn = false;
	}

	counter++;
}

function CreateCard(){
	var randomCardIndex = Random.Range(0,allCards.Length);
	cardPrefab.transform.GetComponent(CardDisplay).card = allCards[randomCardIndex];
	newCard = Instantiate(cardPrefab, Vector3.zero, Quaternion.identity);
	newCard.transform.SetParent(mainCanvas);
	handList.Add(newCard);
}

function CreateCard(position : Vector3){
	var randomCardIndex = Random.Range(0,allCards.Length);
	cardPrefab.transform.GetComponent(CardDisplay).card = allCards[randomCardIndex];
	newCard = Instantiate(cardPrefab, position, Quaternion.identity);
	newCard.transform.SetParent(mainCanvas);
	enemyHandList.Add(newCard);
}

function CreateCard(cardIndex : int){
	cardPrefab.transform.GetComponent(CardDisplay).card = allCards[cardIndex];
	newCard = Instantiate(cardPrefab, Vector3.zero, Quaternion.identity);
	newCard.transform.SetParent(mainCanvas);
	handList.Add(newCard);
}
