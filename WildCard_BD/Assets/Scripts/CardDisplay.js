﻿#pragma strict

import UnityEngine.UI;

var card: Card;

var nameText: Text;
var descriptionText: Text;
var artImage: Image;
var cardbackImage: Image;
var costText: Text;
var attackText: Text;
var hpText: Text;
var canAttack: boolean;

private var canvas: RectTransform;
private var hand: GameObject;
private var enemyHand: GameObject;

function Start () {
	nameText.text = card.name;
	descriptionText.text = card.description;
	artImage.sprite = card.art;

	costText.text = card.cost.ToString();
	attackText.text = card.attack.ToString();
	hpText.text = card.hp.ToString();

	canAttack = card.canAttack;

	transform.position.x = transform.parent.GetComponent(RectTransform).sizeDelta.x + transform.GetComponent(RectTransform).sizeDelta.x;
	transform.rotation.y = 180;

	canvas = transform.parent.GetComponent(RectTransform);
	hand = GameObject.Find("Hand");
	enemyHand = GameObject.Find("EnemyHand");
}

function Update(){
	var cardBack: Image = transform.GetChild(transform.childCount-1).GetComponent(Image);
	if(transform.position.x >= canvas.sizeDelta.x / 2)
	{
		transform.position.x -= 3;
	}
	else
	{
		if(transform.position.y < 1)
		{
			transform.SetParent(hand.transform);
			var EndTurnButton = GameObject.Find("TurnButton");
			EndTurnButton.GetComponent(TurnsHandler).enabled = true;
		}
		else
		{
			transform.SetParent(enemyHand.transform);
		}
		enabled = false;
	}
	if(transform.rotation.y >= 0)
	{
		transform.Rotate(0,-1,0);
	}
	if(transform.rotation.y > 0.7)
	{
		cardBack.enabled = true;
	}
	else
	{
		if(transform.position.y <1)
		{
			cardBack.enabled = false;
		}
	}	
}