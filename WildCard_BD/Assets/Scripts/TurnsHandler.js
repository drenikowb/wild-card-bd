﻿#pragma strict

import UnityEngine.EventSystems;

var manaPrefab: GameObject;
var Hero: GameObject;
var EnemyHero: GameObject;


private var maximumMana: int = 10;

public class TurnsHandler extends MonoBehaviour implements IPointerClickHandler
{
	function OnPointerClick(eventData : PointerEventData)
	{
				
		for(var i: int = 0; i < Manager.totalMana; i++)
		{
			var manaToBeChanged: Transform = GameObject.Find("ManaTray").transform.GetChild(i);
			manaToBeChanged.GetComponent(Image).sprite = manaPrefab.GetComponent(Image).sprite;
		}
		

		
		var board: GameObject = GameObject.Find("Board");
		for(var j: int = 0; j < board.transform.childCount; j++)
		{
			board.transform.GetChild(j).GetComponent(CardDisplay).canAttack = true;
		}

		var sdl: GameObject = GameObject.Find("SaloonDoorLeft");
		sdl.GetComponent(Image).color = Color.white;
		sdl.GetComponent(CanvasGroup).blocksRaycasts = true;
		var sdr: GameObject = GameObject.Find("SaloonDoorRight");
		sdr.GetComponent(Image).color = Color.white;
		sdr.GetComponent(CanvasGroup).blocksRaycasts = true;
		swapSeats();
		if(GameObject.Find("ManaTray").transform.childCount < maximumMana && GameObject.Find("Hero").transform.position.y < 200)
		{
			Manager.totalMana ++;
			var newMana = Instantiate(manaPrefab, Vector3.zero,Quaternion.identity);
			newMana.transform.SetParent(GameObject.Find("ManaTray").transform);
		}
		Manager.manaAvailable = Manager.totalMana;
		enabled = false;
	}

	function swapSeats()
	{
		var tempHeroPosition = GameObject.Find("Hero").transform.position;
		var tempEnemyHeroPosition = GameObject.Find("EnemyHero").transform.position;
		Hero.transform.position = tempEnemyHeroPosition;
		EnemyHero.transform.position = tempHeroPosition;

		var hand = GameObject.Find("Hand");
		var swapHandObject: GameObject;
		swapHandObject = new GameObject ("SwapHand");
		for(var i: int = hand.transform.childCount-1; i > -1; i--)
		{
			hand.transform.GetChild(i).transform.SetParent(swapHandObject.transform);
		}

		var enemyHand = GameObject.Find("EnemyHand");
		var swapEnemyHandObject: GameObject;
		swapEnemyHandObject = new GameObject("SwapEnemyHand");
		for(var j: int = enemyHand.transform.childCount-1; j > -1; j--)
		{
			enemyHand.transform.GetChild(j).transform.SetParent(swapEnemyHandObject.transform);
		}

		for (var k: int = swapHandObject.transform.childCount - 1; k > -1; k--)
		{
			swapHandObject.transform.GetChild(k).transform.SetParent(enemyHand.transform);
		}

		for (var l: int = swapEnemyHandObject.transform.childCount - 1; l > -1; l--)
		{
			swapEnemyHandObject.transform.GetChild(l).transform.SetParent(hand.transform);
		}


		for(var m: int = 0; m < GameObject.Find("Hand").transform.childCount; m++)
		{
			var card = GameObject.Find("Hand").transform.GetChild(m);
			card.GetChild(card.childCount-1).GetComponent(Image).enabled = false;
		}

		for(var n: int = 0; n < GameObject.Find("EnemyHand").transform.childCount; n++)
		{
			var enemyCard = GameObject.Find("EnemyHand").transform.GetChild(n);
			enemyCard.GetChild(enemyCard.childCount-1).GetComponent(Image).enabled = true;
		}

		var board = GameObject.Find("Board");
		var swapBoardObject: GameObject;
		swapBoardObject = new GameObject("SwapBoard");

		for (var o: int = board.transform.childCount-1; o > -1; o--)
		{
			board.transform.GetChild(o).transform.SetParent(swapBoardObject.transform);
		}

		var enemyBoard = GameObject.Find("EnemyBoard");
		var swapEnemyBoardObject: GameObject;
		swapEnemyBoardObject = new GameObject("SwapEnemyBoard");

		for (var p: int = enemyBoard.transform.childCount-1; p > -1; p--)
		{
			enemyBoard.transform.GetChild(p).transform.SetParent(swapEnemyBoardObject.transform);
		}

		for (var q: int = swapBoardObject.transform.childCount - 1; q > -1; q--)
		{
			swapBoardObject.transform.GetChild(q).transform.SetParent(enemyBoard.transform);
		}

		for (var r: int = swapEnemyBoardObject.transform.childCount - 1; r > -1; r--)
		{
			swapEnemyBoardObject.transform.GetChild(r).transform.SetParent(board.transform);
		}

		Destroy(swapHandObject);
		Destroy(swapEnemyHandObject);
		Destroy(swapBoardObject);
		Destroy(swapEnemyBoardObject);
	}
}