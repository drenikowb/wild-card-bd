﻿#pragma strict

import UnityEngine.EventSystems;

public class MouseInput extends MonoBehaviour implements IBeginDragHandler,IDragHandler,IEndDragHandler, IPointerEnterHandler, IPointerExitHandler, IPointerClickHandler
{
	@HideInInspector
	var ParentToReturnTo: Transform;
	@HideInInspector
	var PlaceholderParent: Transform;
	@HideInInspector
	static var dragging: boolean;
	
	@HideInInspector
	static var attacking: boolean;

	var BigCardPrefab: GameObject;
	private var bigCard: GameObject;
	private var placeholder: GameObject;

	function OnBeginDrag(eventData : PointerEventData)
	{
		dragging = true;
		Destroy(bigCard);

		CreatePlaceholder();

		ParentToReturnTo = transform.parent;
		PlaceholderParent = ParentToReturnTo;
		transform.SetParent(transform.parent.parent);
		GetComponent(CanvasGroup).blocksRaycasts = false; 
	}	
	function OnDrag(data : PointerEventData)
	{
		if(dragging)
		{
			var ray = Camera.main.ScreenPointToRay(Input.mousePosition);
			transform.position = new Vector3 (ray.origin.x, ray.origin.y, 0);

			if(placeholder.transform.parent != PlaceholderParent)
			{
				placeholder.transform.SetParent(PlaceholderParent);
			}
			UpdateSiblingIndex();			
		}
	}	
	function OnEndDrag(eventData : PointerEventData)
	{
		dragging = false;
		transform.SetParent(ParentToReturnTo);
		transform.SetSiblingIndex(placeholder.transform.GetSiblingIndex());
		GetComponent(CanvasGroup).blocksRaycasts = true;
		Destroy(placeholder);
	}

	function OnPointerEnter(eventData : PointerEventData)
	{
		if(transform.parent.name == "Hand")
		{
			CreateBigCard();
		}
	}
	function OnPointerExit(eventData : PointerEventData)
	{
		if(!dragging)
		{
			Destroy(bigCard);
		}
	}

	function OnPointerClick(eventData : PointerEventData)
	{
		if(transform.parent.name == "Board" && transform.GetComponent(CardDisplay).canAttack == true)
		{	
			transform.GetChild(transform.childCount-2).GetComponent(Image).enabled = !transform.GetChild(transform.childCount-2).GetComponent(Image).enabled;
		}
		if(transform.parent.name == "EnemyBoard")
		{
			var board = GameObject.Find("Board");
			for (var o: int = board.transform.childCount-1; o > -1; o--)
			{
				if(board.transform.GetChild(o).transform.GetChild(transform.childCount-2).GetComponent(Image).enabled)
				{
					var attackeeHealth = int.Parse(transform.GetComponent(CardDisplay).hpText.text);
					var attackerDamage = int.Parse(board.transform.GetChild(o).transform.GetComponent(CardDisplay).attackText.text);
					attackeeHealth -= attackerDamage;
					var attackerHealth = int.Parse(board.transform.GetChild(o).transform.GetComponent(CardDisplay).hpText.text);
					var attackeeDamage = int.Parse(transform.GetComponent(CardDisplay).attackText.text);
					attackerHealth -= attackeeDamage;
					if(attackeeHealth < 1)
					{
						Destroy(gameObject); 
					}
					if(attackerHealth < 1)
					{
						Destroy(board.transform.GetChild(o).gameObject);
					}
					transform.GetChild(transform.childCount-3).GetComponent(Text).color = new Color(1,0,0);
					board.transform.GetChild(o).transform.GetChild(transform.childCount-3).GetComponent(Text).color = new Color(1,0,0);
					transform.GetComponent(CardDisplay).hpText.text = attackeeHealth.ToString();
					board.transform.GetChild(o).transform.GetChild(transform.childCount-2).GetComponent(Image).enabled = !board.transform.GetChild(o).transform.GetChild(transform.childCount-2).GetComponent(Image).enabled;
					board.transform.GetChild(o).transform.GetComponent(CardDisplay).canAttack = false;
					board.transform.GetChild(o).transform.GetComponent(CardDisplay).hpText.text = attackerHealth.ToString();
				}		
			}
		}
	}

	function CreatePlaceholder()
	{
		placeholder = new GameObject();
		placeholder.transform.SetParent(transform.parent);
		var le = placeholder.AddComponent(LayoutElement);
		le.preferredWidth = GetComponent(LayoutElement).preferredWidth;
		le.preferredHeight = GetComponent(LayoutElement).preferredHeight;
		le.flexibleWidth = 0;
		le.flexibleHeight = 0;
		placeholder.transform.SetSiblingIndex(transform.GetSiblingIndex());
	}

	function UpdateSiblingIndex()
	{
		var newSiblingIndex = PlaceholderParent.childCount;

		for(var i: int = 0; i < PlaceholderParent.childCount; i++)
		{
			if(transform.position.x < PlaceholderParent.GetChild(i).position.x)
			{
				newSiblingIndex = i;
				if(placeholder.transform.GetSiblingIndex() < newSiblingIndex)
				{
					newSiblingIndex--;
				}
				break;
			}
		}

		placeholder.transform.SetSiblingIndex(newSiblingIndex);
	}

	function CreateBigCard()
	{
		if(!dragging)
		{
			BigCardPrefab.transform.GetComponent(CardDisplay).card = transform.GetComponent(CardDisplay).card;
			var offset = Vector3(transform.position.x,transform.position.y + 200,0);
			bigCard = Instantiate(BigCardPrefab, offset, Quaternion.identity);
			var canvas = GameObject.Find("Canvas");
			bigCard.transform.SetParent(canvas.transform);
			bigCard.transform.localScale = Vector3(1.0f,1.0f,1.0f);
		}
	}
}
