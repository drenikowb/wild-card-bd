﻿#pragma strict
import UnityEngine.EventSystems;
public class RemoveSaloonDoor extends MonoBehaviour implements IPointerClickHandler
{
	function OnPointerClick(eventData : PointerEventData)
	{
		var sdl: GameObject = GameObject.Find("SaloonDoorLeft");
		sdl.GetComponent(Image).color = Color.clear;
		sdl.GetComponent(CanvasGroup).blocksRaycasts = false;
		var sdr: GameObject = GameObject.Find("SaloonDoorRight");
		sdr.GetComponent(Image).color = Color.clear;
		sdr.GetComponent(CanvasGroup).blocksRaycasts = false;
		Manager.NewTurn = true;
	}
}