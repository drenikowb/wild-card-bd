﻿#pragma strict

@script CreateAssetMenu(fileName = "New Card", menuName = "Card")
public class Card extends ScriptableObject
{
	public var title: String;
	public var description: String;
	public var art: Sprite;
	public var cost: int;
	public var attack: int;
	public var hp: int;
	public var canAttack: boolean;
}