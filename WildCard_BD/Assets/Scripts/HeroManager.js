﻿#pragma strict

import UnityEngine.EventSystems;

var YouWinScreenPrefab: GameObject;

private var YouWinScreen: GameObject;

public class HeroManager extends MonoBehaviour implements IPointerClickHandler
{
	function OnPointerClick(eventData : PointerEventData)
	{
		var board: GameObject = GameObject.Find("Board");
		for(var i: int = 0; i < board.transform.childCount; i++)
		{
			if(board.transform.GetChild(i).GetChild(board.transform.GetChild(i).childCount-2).GetComponent(Image).enabled)
			{
				var heroHealth: int = int.Parse(transform.GetChild(transform.childCount-1).GetComponent(Text).text);
				heroHealth -= board.transform.GetChild(i).GetComponent(CardDisplay).card.attack;
				board.transform.GetChild(i).GetComponent(CardDisplay).canAttack = false;
				transform.GetChild(transform.childCount-1).GetComponent(Text).text = heroHealth.ToString();				
				board.transform.GetChild(i).GetChild(board.transform.GetChild(i).childCount-2).GetComponent(Image).enabled = false;
			}
		}
	}

	function Update()
	{
		var heroHealth: int = int.Parse(transform.GetChild(transform.childCount-1).GetComponent(Text).text);
		if (heroHealth <= 0)
		{
			var canvas = GameObject.Find("Canvas");
			YouWinScreen = Instantiate(YouWinScreenPrefab, Vector3(canvas.transform.position.x,canvas.transform.position.y,0), Quaternion.identity);
			YouWinScreen.transform.SetParent(canvas.transform);
			var loser = "Hero";
			if (transform.name == "Hero")
			{
				loser = "Enemy Hero";
			}
			YouWinScreen.transform.GetChild(1).GetChild(0).GetComponent(Text).text = "You Win " + loser + "!";
			enabled = false;
		}
		else if(heroHealth < 30)
		{
			transform.GetChild(transform.childCount-1).GetComponent(Text).color = new Color(1,0,0);
		}
		else if(heroHealth > 30)
		{
			transform.GetChild(transform.childCount-1).GetComponent(Text).color = new Color(0,1,0);
		}
		else
		{
			transform.GetChild(transform.childCount-1).GetComponent(Text).color = new Color(1,1,1);
		}
	}
}