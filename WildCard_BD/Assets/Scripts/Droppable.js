﻿#pragma strict

import UnityEngine.EventSystems;

var UsedMana: Sprite;

private var maximumBoardSize: int = 7;

public class Droppable extends MonoBehaviour implements IDropHandler, IPointerEnterHandler, IPointerExitHandler
{
	function OnDrop(eventData : PointerEventData)
	{
		if(MouseInput.dragging == false)
		{
			return;
		}		

		var mI: MouseInput = eventData.pointerDrag.GetComponent(MouseInput);
		if(mI != null)
		{
			mI.ParentToReturnTo = this.transform;
		}
		var cD: CardDisplay = eventData.pointerDrag.GetComponent(CardDisplay);
		AdjustMana(cD);	
	}

	function OnPointerEnter(eventData : PointerEventData)
	{
		if(eventData.pointerDrag == null)
		{
			return;
		}
		var mI: MouseInput = eventData.pointerDrag.GetComponent(MouseInput);
		if(mI != null)
		{
			mI.PlaceholderParent = this.transform;
		}
	}

	function OnPointerExit(eventData : PointerEventData)
	{
		if(eventData.pointerDrag == null)
		{
			return;
		}		
		var cD: CardDisplay = eventData.pointerDrag.GetComponent(CardDisplay);
		var mI: MouseInput = eventData.pointerDrag.GetComponent(MouseInput);
		var board: GameObject = GameObject.Find("Board");
		if(cD.card.cost > Manager.manaAvailable || board.transform.childCount >= maximumBoardSize)
		{
			eventData.pointerDrag.transform.SetParent(mI.ParentToReturnTo);
			MouseInput.dragging = false;
		}
		if(mI != null && mI.PlaceholderParent==this.transform)
		{
			mI.PlaceholderParent = mI.ParentToReturnTo;
		}
	}

	function AdjustMana(cD : CardDisplay)
	{
		if(cD.card.cost <= Manager.manaAvailable && this.name == "Board")
		{
			for(var i: int = Manager.totalMana-1; i >= Manager.manaAvailable-cD.card.cost; i--)
			{
				var manaToBeChanged: Transform = GameObject.Find("ManaTray").transform.GetChild(i);
				manaToBeChanged.GetComponent(Image).sprite = UsedMana;
			}
			Manager.manaAvailable -= cD.card.cost;
		}
	}
}
